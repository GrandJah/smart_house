use crate::smart_house::{DeviceInfoProvider, SmartHouse};
use std::ops::Add;

pub fn create_report_vec(house: &SmartHouse, providers: &[&dyn DeviceInfoProvider]) -> String {
    let mut report = String::from("Report for \"")
        .add(house.name.as_str())
        .add("\"\n");

    house.get_rooms().iter().for_each(|room_name| {
        report.push_str("room:");
        report.push_str(room_name);
        report.push('\n');
        house.devices(room_name).iter().for_each(|device_name| {
            report.push_str("\tdevice_name: \"");
            report.push_str(device_name);
            report.push_str("\" status: ");
            let mut errors = Vec::new();
            let mut states = Vec::new();
            match house.find_device(room_name, device_name) {
                Ok(id) => {
                    providers
                        .iter()
                        .map(|provider| provider.report(id))
                        .for_each(|result| match result {
                            Ok(state) => states.push(state),
                            Err(error) => errors.push(error),
                        });
                    match (states.len(), errors.len()) {
                        (0, 0) => {
                            report.push_str("Not found Info Providers");
                        }
                        (0, _) => {
                            report.push_str(errors[0]);
                        }
                        (_, _) => {
                            report.push_str(states.join("\n").as_str());
                        }
                    }
                    report.push('\n');
                }
                Err(err) => report.push_str(err),
            }
        })
    });
    report.push_str("==\n");
    report
}
#[cfg(test)]
mod tests {
    use crate::smart_dev::PowerSocket;
    use crate::smart_house::*;
    #[test]
    fn borrow_report() {
        let mut socket2 = SmartPowerSocket::new("socket");
        socket2.on();
        let thermo = SmartThermometer::new("thermo");

        let mut house = SmartHouse::new("My smart house", vec!["room1", "room2"]);
        house.add_device_rooms("room1", &socket2).unwrap();
        house.add_device_rooms("room2", &thermo).unwrap();

        let info_provider_2 = BorrowingDeviceInfoProvider::new(&mut socket2, &thermo);

        // Строим отчёт с использованием `BorrowingDeviceInfoProvider`.
        let report = house.create_report(&info_provider_2);
        assert_eq!(
            "Report for \"My smart house\"\n\
        room:room1\n\t\
        device_name: \"socket\" status: power: 800\n\
        room:room2\n\t\
        device_name: \"thermo\" status: temp:20\n\
        ==\n",
            report
        );
    }
    #[test]
    fn own_report() {
        let mut house = SmartHouse::new("My smart house", vec!["room1", "room2"]);

        let mut socket = SmartPowerSocket::new("socket");
        socket.on();
        house.add_device_rooms("room1", &socket).unwrap();

        let info_provider_1 = OwningDeviceInfoProvider::new(socket);
        let report = house.create_report(&info_provider_1);

        assert_eq!(
            "Report for \"My smart house\"\n\
        room:room1\n\t\
        device_name: \"socket\" status: power: 800\n\
        room:room2\n\
        ==\n",
            report
        );
    }
    #[test]
    fn multi_report() {
        let mut house = SmartHouse::new("My smart house", vec!["room1", "room2"]);

        let mut socket1 = SmartPowerSocket::new("socket1");
        socket1.on();
        house.add_device_rooms("room1", &socket1).unwrap();

        let mut socket2 = SmartPowerSocket::new("socket2");
        socket2.on();
        house.add_device_rooms("room2", &socket2).unwrap();

        let thermo = SmartThermometer::new("thermo");
        house.add_device_rooms("room2", &thermo).unwrap();

        let info_provider_1 = OwningDeviceInfoProvider::new(socket1);

        let info_provider_2 = BorrowingDeviceInfoProvider::new(&mut socket2, &thermo);
        let report = create_report!(house, &info_provider_1, &info_provider_2);

        assert_eq!(
            "Report for \"My smart house\"\n\
        room:room1\n\t\
        device_name: \"socket1\" status: power: 800\n\
        room:room2\n\t\
        device_name: \"socket2\" status: power: 800\n\t\
        device_name: \"thermo\" status: temp:20\n\
        ==\n",
            report
        );
    }
}
