use crate::smart_dev::{InfoDevice, next_id, Thermometer};

#[derive(Debug)]
pub struct SmartThermometer {
    id: String,
    name: String,
}
impl SmartThermometer {
    pub fn new(name: &str) -> Self {
        SmartThermometer {
            id: next_id(),
            name: name.to_string(),
        }
    }
}
impl InfoDevice for SmartThermometer {
    fn id(&self) -> String {
        self.id.clone()
    }
    fn name(&self) -> String {
        self.name.clone()
    }
    fn text(&self) -> String {
        format!("temp:{}", self.current_temp())
    }
}
impl Thermometer for SmartThermometer {
    fn current_temp(&self) -> u32 {
        20
    }
}
