use std::net::SocketAddr;
use std::sync::{Arc, RwLock};
use std::time::Duration;

use rand::random;
use tokio::net::UdpSocket;
use tokio::spawn;
use tokio::time::sleep;

use crate::smart_dev::{InfoDevice, next_id, Thermometer};

#[derive(Debug)]
pub struct AutoThermometer {
    id: String,
    name: String,
    temp: Arc<RwLock<i32>>,
    addrs: Arc<RwLock<Vec<SocketAddr>>>,
}
impl AutoThermometer {
    pub fn new(name: &str, secs: Duration) -> Self {
        let temp = Arc::new(RwLock::new(200));
        let ref_temp = Arc::clone(&temp);
        spawn(async move {
            loop {
                sleep(secs).await;
                if let Ok(mut temp) = ref_temp.write() {
                    *temp += (50 - *temp + (random::<u32>() % 250) as i32) / 10;
                }
            }
        });
        AutoThermometer {
            id: next_id(),
            name: name.to_string(),
            temp,
            addrs: Arc::new(RwLock::new(Vec::new())),
        }
    }

    pub async fn udp_start(&self, socket: UdpSocket, duration: Duration) {
        let ref_temp = Arc::clone(&self.temp);
        let ref_addrs = Arc::clone(&self.addrs);
        loop {
            let temp = if let Ok(temp) = ref_temp.read() {
                (*temp / 10) as u32
            } else {
                println!("Error read temp; id:{}", self.id);
                break;
            };
            let address = if let Ok(vec) = ref_addrs.read() {
                vec.iter().copied().collect::<Vec<SocketAddr>>()
            } else {
                println!("Error read connection collection; id:{}", self.id);
                break;
            };

            for addr in address.iter() {
                if let Err(err) = socket.send_to(temp.to_be_bytes().as_slice(), addr).await {
                    println!("Error sen udp data: {}", err);
                }
            }
            sleep(duration).await;
        }
    }

    pub fn add_addr(&self, addr: SocketAddr) {
        if let Ok(mut vec) = self.addrs.write() {
            vec.push(addr);
        }
    }
}
impl InfoDevice for AutoThermometer {
    fn id(&self) -> String {
        self.id.clone()
    }
    fn name(&self) -> String {
        self.name.clone()
    }
    fn text(&self) -> String {
        format!("temp:{}", self.current_temp())
    }
}
impl Thermometer for AutoThermometer {
    fn current_temp(&self) -> u32 {
        (*self.temp.read().unwrap() / 10) as u32
    }
}
