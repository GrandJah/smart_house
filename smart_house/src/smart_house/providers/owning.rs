use crate::smart_dev::InfoDevice;
use crate::smart_house::power_socket::SmartPowerSocket;
use crate::smart_house::providers::DeviceInfoProvider;

pub struct OwningDeviceInfoProvider {
    socket: SmartPowerSocket,
}

impl OwningDeviceInfoProvider {
    pub fn new(socket: SmartPowerSocket) -> Self {
        OwningDeviceInfoProvider { socket }
    }
}

impl DeviceInfoProvider for OwningDeviceInfoProvider {
    fn report(&self, id_device: &str) -> Result<String, &str> {
        if self.socket.id() == id_device {
            Ok(self.socket.text())
        } else {
            Err("Not Support")
        }
    }
}
