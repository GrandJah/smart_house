pub use borrowing::BorrowingDeviceInfoProvider;
pub use default_device_storage::create_device;
pub use default_device_storage::DefaultDeviceInfoProvider;
pub use default_device_storage::DeviceType;
pub use owning::OwningDeviceInfoProvider;

mod borrowing;
mod default_device_storage;
mod owning;

pub trait DeviceInfoProvider: Send {
    fn report(&self, _id_device: &str) -> Result<String, &str> {
        Err("not connect")
    }
}
