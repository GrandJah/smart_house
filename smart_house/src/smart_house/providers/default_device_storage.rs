use std::cell::RefCell;
use std::sync::{Arc, Mutex};
use std::time::Duration;

use once_cell::sync::Lazy;

use crate::smart_dev::{InfoDevice, next_id};
use crate::smart_house::{AutoThermometer, DeviceInfoProvider, SmartPowerSocket, SmartThermometer};

type Storage<T> = Mutex<RefCell<Vec<Arc<T>>>>;
static DEVICE_STORAGE: Lazy<Storage<dyn InfoDevice>> =
    Lazy::new(|| Mutex::new(RefCell::new(Vec::new())));

pub enum DeviceType {
    Unknown,
    SmartThermometer,
    SmartPowerSocket,
    AutoThermometer,
}

pub fn create_device(type_device: DeviceType, name: String) -> Result<Arc<dyn InfoDevice>, String> {
    let device: Arc<dyn InfoDevice> = match type_device {
        DeviceType::SmartPowerSocket => Arc::new(SmartPowerSocket::new(name.as_str())),
        DeviceType::SmartThermometer => Arc::new(SmartThermometer::new(name.as_str())),
        DeviceType::AutoThermometer => {
            Arc::new(AutoThermometer::new(name.as_str(), Duration::from_secs(5)))
        }
        _ => Arc::new(UnknownDevice::new(name)),
    };
    match DEVICE_STORAGE.lock() {
        Ok(s) => s.borrow_mut().push(Arc::clone(&device)),
        Err(w) => return Err(w.to_string()),
    };
    Ok(device)
}

struct UnknownDevice {
    id: String,
    name: String,
}

impl UnknownDevice {
    pub fn new(name: String) -> Self {
        let id = next_id();
        Self { id, name }
    }
}

impl InfoDevice for UnknownDevice {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn name(&self) -> String {
        self.name.clone()
    }

    fn text(&self) -> String {
        "Unknown Device".to_string()
    }
}

pub struct DefaultDeviceInfoProvider;

impl DeviceInfoProvider for DefaultDeviceInfoProvider {
    fn report(&self, id_device: &str) -> Result<String, &str> {
        match DEVICE_STORAGE.lock() {
            Ok(mutex) => {
                return match mutex.borrow().iter().find(|d| d.id() == id_device) {
                    None => Err("Not Found"),
                    Some(device_info) => Ok(device_info.text()),
                };
            }
            Err(_) => Err("Internal Error"),
        }
    }
}
