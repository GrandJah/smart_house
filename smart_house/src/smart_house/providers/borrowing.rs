use crate::smart_dev::InfoDevice;
use crate::smart_house::power_socket::SmartPowerSocket;
use crate::smart_house::providers::DeviceInfoProvider;
use crate::smart_house::thermometer::SmartThermometer;

pub struct BorrowingDeviceInfoProvider<'a, 'b> {
    socket: &'a mut SmartPowerSocket,
    thermo: &'b SmartThermometer,
}

impl<'a, 'b> BorrowingDeviceInfoProvider<'a, 'b> {
    pub fn new(socket: &'a mut SmartPowerSocket, thermo: &'b SmartThermometer) -> Self {
        BorrowingDeviceInfoProvider { socket, thermo }
    }
}

impl<'a, 'b> DeviceInfoProvider for BorrowingDeviceInfoProvider<'a, 'b> {
    fn report(&self, id_device: &str) -> Result<String, &str> {
        if self.socket.id() == id_device {
            Ok(self.socket.text())
        } else if self.thermo.id() == id_device {
            Ok(self.thermo.text())
        } else {
            Err("Not Support")
        }
    }
}
