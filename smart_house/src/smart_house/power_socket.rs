use std::cell::RefCell;
use std::sync::{Arc, Mutex};

use crate::smart_dev::{InfoDevice, PowerSocket};
use crate::smart_house::SmartPowerSocketHandle;

#[derive(Debug)]
pub struct SmartPowerSocket {
    socket: Arc<Mutex<RefCell<SmartPowerSocketHandle>>>,
}

impl SmartPowerSocket {
    pub fn new(name: &str) -> Self {
        SmartPowerSocket {
            socket: Arc::new(Mutex::new(RefCell::new(SmartPowerSocketHandle::new(name)))),
        }
    }

    pub fn clone_socket(&self) -> Arc<Mutex<RefCell<SmartPowerSocketHandle>>> {
        Arc::clone(&self.socket)
    }
}

impl InfoDevice for SmartPowerSocket {
    fn id(&self) -> String {
        self.socket.lock().unwrap().borrow().id()
    }
    fn name(&self) -> String {
        self.socket.lock().unwrap().borrow().name()
    }
    fn text(&self) -> String {
        self.socket.lock().unwrap().borrow().text()
    }
}
impl PowerSocket for SmartPowerSocket {
    fn on(&mut self) {
        self.socket.lock().unwrap().borrow_mut().on();
    }
    fn off(&mut self) {
        self.socket.lock().unwrap().borrow_mut().off();
    }
    fn current_power(&self) -> u32 {
        self.socket.lock().unwrap().borrow().current_power()
    }
}
