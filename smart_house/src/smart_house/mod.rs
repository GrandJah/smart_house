use crate::create_report;
use crate::smart_dev::InfoDevice;
pub use crate::smart_house::create_report::create_report_vec;
pub use crate::smart_house::power_socket::SmartPowerSocket;
pub use crate::smart_house::power_socket_handle::SmartPowerSocketHandle;
pub use crate::smart_house::providers::create_device;
pub use crate::smart_house::providers::{
    BorrowingDeviceInfoProvider, DefaultDeviceInfoProvider, DeviceInfoProvider, DeviceType,
    OwningDeviceInfoProvider,
};
pub use crate::smart_house::thermometer::SmartThermometer;
pub use crate::smart_house::udp_thermometer::AutoThermometer;

mod create_report;
mod power_socket;
mod power_socket_handle;
mod providers;
mod thermometer;
mod udp_thermometer;

pub struct SmartHouse {
    name: String,
    rooms: Vec<SmartRoom>,
}
struct SmartRoom {
    name: String,
    devices: Vec<SmartDev>,
}
struct SmartDev {
    id: String,
    name: String,
}

#[macro_export]
macro_rules! create_report {
    ($house:expr,$($args:expr),*) => {{
        use $crate::smart_house::DeviceInfoProvider;
        use $crate::smart_house::create_report_vec;
        let providers: Vec<&dyn DeviceInfoProvider> = vec![$($args,)*];
        create_report_vec(&$house,&providers)
    }}
}

impl SmartHouse {
    pub fn new(name: &str, rooms: Vec<&str>) -> Self {
        SmartHouse {
            name: name.to_string(),
            rooms: rooms
                .iter()
                .map(|s| SmartRoom {
                    name: s.to_string(),
                    devices: Vec::new(),
                })
                .collect(),
        }
    }
    pub fn get_rooms(&self) -> Vec<&str> {
        self.rooms.iter().map(|room| room.name.as_str()).collect()
    }

    pub fn add_room(&mut self, room_name: &str) -> Result<(), String> {
        match self
            .rooms
            .iter_mut()
            .find(|room| room.name.as_str() == room_name)
        {
            None => {
                self.rooms.push(SmartRoom {
                    name: room_name.to_string(),
                    devices: Vec::new(),
                });
                Ok(())
            }
            Some(_) => Err(format!("Room with name {room_name} is exist")),
        }
    }

    pub fn remove_room(&mut self, room_name: &str) -> Result<(), String> {
        match self
            .rooms
            .iter_mut()
            .enumerate()
            .find(|(_, room)| room.name.as_str() == room_name)
        {
            None => Err(format!("Room with name {room_name} not exist")),
            Some((idx, _)) => {
                self.rooms.remove(idx);
                Ok(())
            }
        }
    }

    pub fn devices(&self, room: &str) -> Vec<String> {
        self.rooms
            .iter()
            .find(|find_room| find_room.name.as_str() == room)
            .iter()
            .flat_map(|room| room.devices.iter().map(|device| device.name.to_string()))
            .collect()
    }
    pub fn create_report(&self, device: &impl DeviceInfoProvider) -> String {
        create_report!(self, device)
    }
    pub fn add_device_rooms(
        &mut self,
        room_name: &str,
        device: &dyn InfoDevice,
    ) -> Result<(), &str> {
        match self
            .rooms
            .iter_mut()
            .find(|room| room.name.as_str() == room_name)
        {
            None => Err("Not found room for name"),
            Some(room) => {
                room.devices.push(SmartDev {
                    name: device.name().clone(),
                    id: device.id().clone(),
                });
                Ok(())
            }
        }
    }

    pub fn remove_device_rooms(&mut self, room_name: &str, device_name: &str) -> Result<(), &str> {
        let room = self
            .rooms
            .iter_mut()
            .find(|room| room.name.as_str() == room_name)
            .ok_or("Room not found")?;
        let (idx, _) = room
            .devices
            .iter()
            .enumerate()
            .find(|(_, dev)| dev.name.as_str() == device_name)
            .ok_or("Device not found")?;
        room.devices.remove(idx);
        Ok(())
    }

    pub fn find_device(&self, room_name: &str, device_name: &str) -> Result<&str, &str> {
        let room = self
            .rooms
            .iter()
            .find(|room| room.name.as_str() == room_name)
            .ok_or("Room not found")?;
        let dev = room
            .devices
            .iter()
            .find(|dev| dev.name.as_str() == device_name)
            .ok_or("Device not found")?;
        Ok(dev.id.as_str())
    }
}

#[cfg(test)]
mod test {
    use crate::smart_house::SmartHouse;

    #[test]
    fn add_new_room() {
        let mut house = SmartHouse::new("My smart house", vec!["room1", "room2"]);
        let _ignored = house.add_room("room3");
        assert_eq!(house.rooms.len(), 3);
    }

    #[test]
    fn add_exist_room() {
        let mut house = SmartHouse::new("My smart house", vec!["room1", "room2"]);
        let res = house.add_room("room2").err().unwrap();
        assert_eq!(res, "Room with name room2 is exist");
        assert_eq!(house.rooms.len(), 2);
    }
}
