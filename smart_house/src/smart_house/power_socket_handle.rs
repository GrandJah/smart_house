use crate::smart_dev::{InfoDevice, next_id, PowerSocket};

const DEFAULT_POWER: u32 = 800;

#[derive(Debug)]
pub struct SmartPowerSocketHandle {
    id: String,
    name: String,
    power: u32,
}

impl SmartPowerSocketHandle {
    pub fn new(name: &str) -> Self {
        SmartPowerSocketHandle {
            id: next_id(),
            name: name.to_string(),
            power: 0,
        }
    }
}

impl InfoDevice for SmartPowerSocketHandle {
    fn id(&self) -> String {
        self.id.clone()
    }
    fn name(&self) -> String {
        self.name.clone()
    }
    fn text(&self) -> String {
        format!("power: {}", self.current_power())
    }
}
impl PowerSocket for SmartPowerSocketHandle {
    fn on(&mut self) {
        self.power = DEFAULT_POWER;
        println!("Socket-{} ON", self.id);
    }
    fn off(&mut self) {
        self.power = 0;
        println!("Socket-{} OFF", self.id);
    }
    fn current_power(&self) -> u32 {
        self.power
    }
}

pub trait PowerSocketProvider<'a> {
    fn find_power_socket(
        &'a mut self,
        _id_device: &str,
    ) -> Result<&'a mut SmartPowerSocketHandle, &'static str> {
        Err("not found socket")
    }
}
