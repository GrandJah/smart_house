use uuid::Uuid;

pub fn next_id() -> String {
    Uuid::new_v4().to_string()
}
pub trait InfoDevice: Send + Sync {
    fn id(&self) -> String;
    fn name(&self) -> String;
    fn text(&self) -> String;
}
pub trait PowerSocket: InfoDevice {
    fn on(&mut self);
    fn off(&mut self);
    fn current_power(&self) -> u32;
}
pub trait Thermometer: InfoDevice {
    fn current_temp(&self) -> u32;
}
