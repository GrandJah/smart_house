use std::net::SocketAddr;
use std::str::FromStr;
use std::time::Duration;
use tokio::join;
use tokio::net::UdpSocket;

use smart_house::smart_house::{AutoThermometer, SmartHouse};

const ADDR_SOCKET: &str = "127.0.0.1:1234";
const ADDR_CLIENT_1: &str = "127.0.0.2:1234";
const ADDR_CLIENT_2: &str = "127.0.0.3:1234";

async fn udp_client(addr: &str) {
    let socket = UdpSocket::bind(addr).await.unwrap();
    let mut buf = [0u8; 4];
    loop {
        match socket.recv(&mut buf).await {
            Ok(_) => {
                println!("{} thermo: {}", addr, u32::from_be_bytes(buf))
            }
            Err(err) => {
                println!("{}", err)
            }
        }
    }
}

#[tokio::main]
async fn main() {
    async_main().await;
}

async fn async_main() {
    let mut house = SmartHouse::new("My smart house", vec!["room"]);
    let thermo = AutoThermometer::new("thermo", Duration::from_secs(2));
    house.add_device_rooms("room", &thermo).unwrap();
    thermo.add_addr(SocketAddr::from_str(ADDR_CLIENT_1).unwrap());
    thermo.add_addr(SocketAddr::from_str(ADDR_CLIENT_2).unwrap());
    join!(
        thermo.udp_start(
            UdpSocket::bind(ADDR_SOCKET).await.unwrap(),
            Duration::from_secs(1),
        ),
        udp_client(ADDR_CLIENT_1),
        udp_client(ADDR_CLIENT_2)
    );
}
