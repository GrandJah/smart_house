use std::cell::RefCell;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use std::{fs, thread};

use thiserror::Error;

use server::{AnyError, Server, API};
use smart_house::smart_dev::{InfoDevice, PowerSocket};
use smart_house::smart_house::{
    create_report_vec, BorrowingDeviceInfoProvider, DeviceInfoProvider, OwningDeviceInfoProvider,
    SmartHouse, SmartPowerSocket, SmartPowerSocketHandle, SmartThermometer,
};

#[tokio::main]
async fn main() {
    async_main().await;
}

async fn async_main() {
    let socket1 = SmartPowerSocket::new("socket1");
    let socket2 = SmartPowerSocket::new("socket2");
    let sockets = vec![socket1.clone_socket(), socket2.clone_socket()];
    let thermo = SmartThermometer::new("thermo");

    let mut house = SmartHouse::new("My smart house", vec!["room1", "room2"]);
    house.add_device_rooms("room1", &socket1).unwrap();
    house.add_device_rooms("room2", &socket2).unwrap();
    house.add_device_rooms("room2", &thermo).unwrap();

    let info_provider_1 = Box::new(OwningDeviceInfoProvider::new(socket1));
    let socket2 = Box::leak(socket2.into());
    let thermo = Box::leak(thermo.into());
    let info_provider_2 = Box::new(BorrowingDeviceInfoProvider::new(socket2, thermo));
    let addr =
        fs::read_to_string("settings/addr").unwrap_or_else(|_| String::from("127.0.0.1:8080"));
    let api = HouseAPI {
        house,
        providers: vec![info_provider_1, info_provider_2],
        sockets,
    };
    let server = Server::bind(addr, api).await;
    drop(server);
}

struct HouseAPI {
    house: SmartHouse,
    providers: Vec<Box<dyn DeviceInfoProvider>>,
    sockets: Vec<Arc<Mutex<RefCell<SmartPowerSocketHandle>>>>,
}

impl HouseAPI {
    fn parsecom(&mut self, command: &str) -> Result<String, AnyError> {
        let mut com = command.split(' ');
        let room_name = com.next().ok_or(unknown(command, 0))?;
        let dev_name = com.next().ok_or(unknown(command, 1))?;
        let action = com.next().ok_or(unknown(command, 2))?;

        match self
            .house
            .find_device(room_name, dev_name)
            .map_err(|e| e.to_string())
        {
            Ok(dev_id) => {
                let mut resp = String::new();
                if let Some(ref_dev) = self
                    .sockets
                    .iter()
                    .find(|ref_dev| ref_dev.lock().unwrap().borrow().id() == dev_id)
                {
                    if let Ok(ref mut socket) = ref_dev.lock().unwrap().try_borrow_mut() {
                        match action {
                            "on" => {
                                socket.on();
                                resp.push_str(
                                    format!("socket {dev_name} in {room_name} - ON").as_str(),
                                );
                            }
                            "off" => {
                                socket.off();
                                resp.push_str(
                                    format!("socket {dev_name} in {room_name} - OFF").as_str(),
                                );
                            }
                            "power" => {
                                let power = socket.current_power();
                                resp.push_str(
                                    format!("socket {dev_name} in {room_name} - power:{power}")
                                        .as_str(),
                                );
                            }
                            oops => resp.push_str(
                                format!(
                                    "socket {dev_name} in {room_name} - command {oops} - not support"
                                )
                                    .as_str(),
                            ),
                        }
                    }
                }
                Ok(resp)
            }
            Err(err) => Ok(format!(
                "error : {room_name} - {dev_name} - {action} - {err}"
            )),
        }
    }
}

#[derive(Debug, Error)]
enum Error {
    #[error("unknown command: {0} - {1}")]
    CommandError(String, u32),
}

fn unknown(command: &str, mode: u32) -> Error {
    Error::CommandError(command.to_string(), mode)
}

impl API for HouseAPI {
    fn handle(&mut self, request: String) -> Result<String, AnyError> {
        thread::sleep(Duration::from_millis(100));
        Ok(match request.as_str() {
            "report" => create_report_vec(
                &self.house,
                self.providers
                    .iter()
                    .map(|d| d.as_ref())
                    .collect::<Vec<&dyn DeviceInfoProvider>>()
                    .as_slice(),
            ),
            com => self.parsecom(com)?,
        })
    }
}
