use smart_house::create_report;
use smart_house::smart_house::{
    BorrowingDeviceInfoProvider, OwningDeviceInfoProvider, SmartHouse, SmartPowerSocket,
    SmartThermometer,
};

fn main() {
    let socket1 = SmartPowerSocket::new("socket1");
    let mut socket2 = SmartPowerSocket::new("socket2");
    let thermo = SmartThermometer::new("thermo");

    // Инициализация дома
    let mut house = SmartHouse::new("My smart house", vec!["room1", "room2"]);
    house.add_device_rooms("room1", &socket1).unwrap();
    house.add_device_rooms("room2", &socket2).unwrap();
    house.add_device_rooms("room2", &thermo).unwrap();

    // Строим отчёт с использованием `OwningDeviceInfoProvider`.
    let info_provider_1 = OwningDeviceInfoProvider::new(socket1);
    let report1 = house.create_report(&info_provider_1);

    // Строим отчёт с использованием `BorrowingDeviceInfoProvider`.
    let info_provider_2 = BorrowingDeviceInfoProvider::new(&mut socket2, &thermo);
    let report2 = house.create_report(&info_provider_2);

    // Выводим отчёты на экран:
    println!("Report #1: {report1}");
    println!("Report #2: {report2}");
    println!(
        "Report: {}",
        create_report!(house, &info_provider_1, &info_provider_2)
    );
}
