use axum_ex::server;

#[tokio::main]
async fn main() {
    server().await;
}
