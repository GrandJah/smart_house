use std::sync::{Arc, Mutex};

use axum::extract::{Path, State};
use axum::Json;
use axum::response::IntoResponse;

use http_api::{DevData, ListRsData, ReportRs, RoomRqData};
use smart_house::smart_house::create_device as create_device_storage;
use smart_house::smart_house::DefaultDeviceInfoProvider;
use smart_house::smart_house::DeviceType::Unknown;
use smart_house::smart_house::SmartHouse;

use crate::error::{CustomResult, ok_resp};

type HouseState = State<Arc<Mutex<SmartHouse>>>;

pub async fn create_room(
    State(house): HouseState,
    Json(data): Json<RoomRqData>,
) -> impl IntoResponse {
    house
        .lock()
        .as_mut()
        .unwrap()
        .add_room(&data.name)
        .map(Json)
}

pub async fn read_rooms(State(house): HouseState) -> impl IntoResponse {
    let rooms: CustomResult<ListRsData> = Ok(ListRsData {
        list: house
            .lock()
            .as_ref()
            .unwrap()
            .get_rooms()
            .iter()
            .map(ToString::to_string)
            .collect(),
    });
    rooms.map(Json)
}

pub async fn read_room_devices(
    Path(room_name): Path<String>,
    State(house): HouseState,
) -> impl IntoResponse {
    let devices: CustomResult<ListRsData> = Ok(ListRsData {
        list: house
            .lock()
            .as_ref()
            .unwrap()
            .devices(room_name.as_ref())
            .iter()
            .map(ToString::to_string)
            .collect(),
    });
    devices.map(Json)
}

pub async fn delete_room(
    Path(room_name): Path<String>,
    State(house): HouseState,
) -> impl IntoResponse {
    house
        .lock()
        .as_mut()
        .unwrap()
        .remove_room(room_name.as_ref())
        .map(Json)
}

pub async fn create_device(
    Path(room_name): Path<String>,
    State(house): HouseState,
    Json(data): Json<DevData>,
) -> impl IntoResponse {
    let dev = create_device_storage(Unknown, data.name)?;
    house
        .lock()
        .as_mut()
        .unwrap()
        .add_device_rooms(room_name.as_ref(), dev.as_ref())
        .map_err(ToString::to_string)
        .map(Json)
}

pub async fn delete_device(
    Path((room_name, device_name)): Path<(String, String)>,
    State(house): HouseState,
) -> impl IntoResponse {
    house
        .lock()
        .as_mut()
        .unwrap()
        .remove_device_rooms(room_name.as_str(), device_name.as_str())
        .map_err(ToString::to_string)
        .map(Json)
}

pub async fn find_device(
    Path((room_name, device_name)): Path<(String, String)>,
    State(house): HouseState,
) -> impl IntoResponse {
    house
        .lock()
        .as_ref()
        .unwrap()
        .find_device(room_name.as_str(), device_name.as_str())
        .map_err(ToString::to_string)
        .map(|id| DevData {
            id: Some(id.to_string()),
            name: device_name,
        })
        .map(Json)
}

pub async fn house_report(State(house): HouseState) -> impl IntoResponse {
    let report = house
        .lock()
        .as_ref()
        .unwrap()
        .create_report(&DefaultDeviceInfoProvider {});

    ok_resp(ReportRs { report }).map(Json)
}
