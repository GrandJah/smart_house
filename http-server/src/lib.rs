use std::env;
use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::{Arc, Mutex};

use axum::routing::get;
use axum::Router;

use smart_house::smart_house::SmartHouse;

use crate::smart_house_client::{
    create_device, create_room, delete_device, delete_room, find_device, house_report,
    read_room_devices, read_rooms,
};

mod error;
mod smart_house_client;

pub async fn server() {
    match dotenv::dotenv() {
        Ok(x) => {
            println!("Load environment from file:{:?}", x)
        }
        Err(x) => {
            if x.not_found() {
                println!("Not found .env file. default settings")
            } else {
                panic!("Error load .env file: {}", x)
            }
        }
    };
    tracing_subscriber::fmt().init();

    let house_name = env::var("HOUSE_NAME").unwrap_or_else(|_| "default name house".to_string());
    let house_ip = env::var("HOUSE_IP").unwrap_or_else(|_| "0.0.0.0".to_string());
    let house_port = env::var("HOUSE_PORT").unwrap_or_else(|_| "8080".to_string());
    let socket_addr =
        SocketAddr::from_str(format!("{}:{}", house_ip, house_port).as_str()).unwrap();

    let house = Arc::new(Mutex::new(SmartHouse::new(house_name.as_ref(), Vec::new())));
    let app = Router::new()
        .route("/", get(house_report))
        .route("/room", get(read_rooms).post(create_room))
        .route("/room/:name", get(read_room_devices).delete(delete_room))
        .route(
            "/room/:name/device",
            get(read_room_devices).post(create_device),
        )
        .route(
            "/room/:name/device/:device_name",
            get(find_device).delete(delete_device),
        )
        .with_state(house);

    let listener = tokio::net::TcpListener::bind(socket_addr).await.unwrap();
    axum::serve(listener, app).await.unwrap();
}
