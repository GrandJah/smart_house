use std::net::SocketAddr;
use std::sync::Arc;
use std::sync::Mutex;

use anyhow::{bail, Error};
use thiserror::Error;
use tokio::net::{TcpListener, TcpStream, ToSocketAddrs};

use smart_protocol::{SmartConnection, SmartProtocol, SmartProtocolResult};

struct TcpConnection {
    connect: SmartConnection<TcpStream>,
    addr: SocketAddr,
}

pub use anyhow::Error as AnyError;

pub trait API: Send {
    fn handle(&mut self, request: String) -> Result<String, AnyError>;
}

impl TcpConnection {
    async fn start(mut self, api: Arc<Mutex<impl API + 'static>>) {
        loop {
            if let Err(err) = self.handle_connection(Arc::clone(&api)).await {
                println!("Client {} - disconnected: {}", self.addr, err);
                break;
            }
        }
    }

    async fn handle_connection(
        &mut self,
        api: Arc<Mutex<impl API + 'static>>,
    ) -> Result<(), Error> {
        let req = self.connect.read_string().await?;
        if req.is_empty() {
            bail!("Empty body request");
        }
        let resp = api.lock().map_err(|e| CustomErrorStruct::CustomError(e.to_string()))?.handle(req)?;
        self.connect.write_string(resp).await?;
        Ok(())
    }
}

pub type CustomResult<T> = Result<T, CustomErrorStruct>;

#[derive(Debug, Error)]
pub enum CustomErrorStruct {
    #[error("CustomError: {0}")]
    CustomError(String),
}

pub struct Server {
    tcp: TcpListener,
}

impl Server {
    pub async fn bind<Addrs>(addrs: Addrs, target: impl API + 'static) -> Result<(), String>
        where
            Addrs: ToSocketAddrs,
    {
        let target = Arc::new(Mutex::new(target));
        let listener = TcpListener::bind(addrs)
            .await
            .map_err(|e| format!("Error bind for: {e}"))?;
        let server = Self { tcp: listener };

        loop {
            match server.accept().await {
                Ok(c) => c.start(Arc::clone(&target)).await,
                Err(e) => {
                    eprintln!("Can't establish connection: {}", e);
                    continue;
                }
            };
        }
    }

    async fn accept(&self) -> SmartProtocolResult<TcpConnection> {
        let (s, addr) = self.tcp.accept().await?;
        println!("New client connected: {}", addr);
        Ok(TcpConnection {
            connect: SmartConnection::server(s).await?,
            addr,
        })
    }
}
