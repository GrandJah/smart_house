use std::fmt::Debug;
use std::string::FromUtf8Error;

use async_trait::async_trait;
use thiserror::Error;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

pub struct SmartConnection<Connection: AsyncWriteExt + AsyncReadExt + Unpin> {
    connection: Connection,
}

pub type SmartProtocolResult<T> = Result<T, SmartProtocolError>;

#[derive(Debug, Error)]
pub enum SmartProtocolError {
    #[error("Convert to UTF8 error: {0}")]
    ConvertToUTF8(#[from] FromUtf8Error),
    #[error("std::io::Error : {0}")]
    StdIoError(#[from] std::io::Error),
    #[error("ProtocolError: {0}")]
    ProtocolError(String),
}

#[async_trait]
pub trait SmartProtocol {
    async fn read_string(&mut self) -> SmartProtocolResult<String>;

    async fn write_string<Data: AsRef<str> + Send>(&mut self, data: Data) -> SmartProtocolResult<()>;

    async fn handle<Data: AsRef<str> + Send>(&mut self, data: Data) -> SmartProtocolResult<String> {
        self.write_string(data).await?;
        self.read_string().await
    }
}

impl<Connection: AsyncWriteExt + AsyncReadExt + Unpin> SmartConnection<Connection> {
    pub async fn new(mut connection: Connection) -> SmartProtocolResult<Self> {
        try_handshake(&mut connection, false).await?;
        Ok(SmartConnection { connection })
    }

    pub async fn server(mut connection: Connection) -> SmartProtocolResult<Self> {
        try_handshake(&mut connection, true).await?;
        Ok(SmartConnection { connection })
    }
}

#[async_trait]
impl<Connection: AsyncWriteExt + AsyncReadExt + Unpin + Send> SmartProtocol
for SmartConnection<Connection>
{
    async fn read_string(&mut self) -> SmartProtocolResult<String> {
        recv_string(&mut self.connection).await
    }

    async fn write_string<Data: AsRef<str> + Send>(&mut self, data: Data) -> SmartProtocolResult<()> {
        send_string(data, &mut self.connection).await
    }
}

async fn try_handshake<Connection: AsyncWriteExt + AsyncReadExt + Unpin>(
    connection: &mut Connection,
    server: bool,
) -> SmartProtocolResult<()> {
    if !server {
        send_string("client", connection).await?;
    };
    let str = recv_string(connection).await?;
    if (server && str != "client") || (!server && str != "server") {
        return Err(SmartProtocolError::ProtocolError(format!("received: {:?}", str)));
    }
    if server {
        send_string("server", connection).await?;
    };
    Ok(())
}

async fn send_string<Data: AsRef<str>, Writer: AsyncWriteExt + Unpin>(
    data: Data,
    writer: &mut Writer,
) -> SmartProtocolResult<()> {
    let bytes = data.as_ref().as_bytes();
    let len = bytes.len() as u32;
    let len_bytes = len.to_be_bytes();
    let _ = writer
        .write_all(&len_bytes)
        .await
        .map_err(|e| e.to_string());
    let _ = writer.write_all(bytes).await.map_err(|e| e.to_string());
    Ok(())
}

async fn recv_string<Reader: AsyncReadExt + Unpin>(reader: &mut Reader) -> SmartProtocolResult<String> {
    let mut buf = [0; 4];
    let _ = reader.read_exact(&mut buf).await?;
    let len = u32::from_be_bytes(buf);

    let mut buf = vec![0; len as _];
    let _ = reader.read_exact(&mut buf).await?;
    Ok(String::from_utf8(buf)?)
}
