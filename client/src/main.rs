use std::fs;
use std::time::Duration;

use tokio::net::{TcpStream, ToSocketAddrs};

use smart_protocol::{SmartConnection, SmartProtocol, SmartProtocolResult};

#[tokio::main]
async fn main() {
    async_main().await;
}

async fn async_main() {
    let addr =
        fs::read_to_string("settings/addr").unwrap_or_else(|_| String::from("127.0.0.1:8080"));
    let mut client = Client::connect(addr).await.unwrap();
    client.action("report").await;
    client.action("room1 socket1 on").await;
    client.action("room1 socket1 power").await;
    client.action("report").await;
    client.action("room1 socket1 off").await;
    client.action("room1 socket1 power").await;
    client.action("report").await;
    tokio::time::sleep(Duration::from_secs(3)).await;
}

pub struct Client {
    connect: SmartConnection<TcpStream>,
}

impl Client {
    pub async fn connect<Addrs>(addrs: Addrs) -> SmartProtocolResult<Self>
    where
        Addrs: ToSocketAddrs,
    {
        Ok(Client {
            connect: SmartConnection::new(
                TcpStream::connect(addrs).await?,
            )
            .await?,
        })
    }

    pub async fn action(&mut self, command: &str) {
        if let Ok(answer) = self.connect.handle(command).await {
            println!("{answer}");
        }
    }
}
