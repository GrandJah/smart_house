use std::fmt::Debug;
use std::fs;
use std::hash::Hash;
use std::pin::Pin;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering::Relaxed;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll, Waker};
use std::time::Duration;

use iced::alignment::Horizontal;
use iced::futures::{Stream, StreamExt};
use iced::widget::{button, column, text};
use iced::{Alignment, Application, Command, Element, Settings, Theme};
use iced_futures::core::Hasher as OtherHasher;
use iced_futures::subscription::{EventStream, Recipe};
use iced_futures::{BoxStream, Subscription};
use tokio::net::{TcpStream, ToSocketAddrs};
use tokio::task;
use tokio::time::sleep;

use smart_protocol::{SmartConnection, SmartProtocol, SmartProtocolError, SmartProtocolResult};

pub fn main() -> iced::Result {
    let mut settings = Settings::default();
    settings.window.size = (120, 150);
    SocketView::run(settings)
}

enum SocketView {
    Connecting,
    Connect(State),
    ErrorConnect(SmartProtocolError),
}

struct State {
    toggle: bool,
    power: u32,
    client: Arc<Client>,
    soc_state: SocState,
}

#[derive(Debug, Clone)]
enum Message {
    Loading(Arc<SmartProtocolResult<Client>>),
    On,
    Off,
    ChangeState((u32, bool)),
}

impl Application for SocketView {
    type Executor = iced_futures::backend::native::tokio::Executor;
    type Message = Message;
    type Theme = Theme;
    type Flags = ();

    fn new(_flags: ()) -> (Self, Command<Message>) {
        (
            SocketView::Connecting,
            Command::perform(connect(), |res| Message::Loading(Arc::new(res))),
        )
    }

    fn title(&self) -> String {
        match self {
            SocketView::Connecting => String::from("Smart House.. connecting"),
            SocketView::Connect(_) => String::from("Smart House"),
            SocketView::ErrorConnect(_) => String::from("Smart House.. Ошибка сервера"),
        }
    }

    fn update(&mut self, message: Message) -> Command<Message> {
        match self {
            SocketView::Connecting => {
                if let Message::Loading(result) = message {
                    match Arc::into_inner(result).unwrap() {
                        Ok(client) => {
                            let client = Arc::new(client);
                            let toggle = false;
                            let power = 0;
                            let soc_state = SocState::new(&client.rx_state);
                            *self = SocketView::Connect(State {
                                power,
                                client,
                                toggle,
                                soc_state,
                            });
                        }
                        Err(err) => {
                            *self = SocketView::ErrorConnect(err);
                        }
                    }
                }
            }
            SocketView::Connect(state) => match message {
                Message::On => {
                    state.client.tx_com.send(ComSocket::On).unwrap();
                }
                Message::Off => {
                    state.client.tx_com.send(ComSocket::Off).unwrap();
                }
                Message::ChangeState(message) => {
                    state.power = message.0;
                    state.toggle = message.1;
                }
                _ => {}
            },
            SocketView::ErrorConnect(_) => {}
        }
        Command::none()
    }

    fn view(&self) -> Element<Message> {
        match self {
            SocketView::Connecting => {
                let _connecting_text = text("Connect to server").size(20);
                column![_connecting_text]
                    .padding(20)
                    .align_items(Alignment::Center)
                    .into()
            }
            SocketView::Connect(state) => {
                let button_state = if state.toggle {
                    ("Off", Message::Off)
                } else {
                    ("On", Message::On)
                };

                let button_text = text(button_state.0)
                    .size(20)
                    .width(100)
                    .horizontal_alignment(Horizontal::Center);

                let _button = button(button_text).on_press(button_state.1).width(100);
                let _power_text = text(state.power)
                    .size(50)
                    .width(100)
                    .horizontal_alignment(Horizontal::Center);
                column![_power_text, _button]
                    .padding(20)
                    .align_items(Alignment::Center)
                    .into()
            }
            SocketView::ErrorConnect(err) => {
                let message = format!("Sorry. Connect to server error: {}", err);
                let _error_text = text(message).size(10);
                column![_error_text]
                    .padding(20)
                    .align_items(Alignment::Center)
                    .into()
            }
        }
    }

    fn subscription(&self) -> Subscription<Self::Message> {
        match self {
            SocketView::Connect(state) => Subscription::from_recipe(state.soc_state.clone()),
            _ => Subscription::none(),
        }
    }
}

#[derive(Clone, Debug)]
struct SocState {
    state: Arc<Mutex<Option<(u32, bool)>>>,
    waker: Arc<Mutex<Option<Waker>>>,
}

impl SocState {
    fn new(rx_state: &Arc<Mutex<Receiver<(u32, bool)>>>) -> Self {
        let state = Arc::new(Mutex::new(None));
        let waker = Arc::new(Mutex::new(None));
        task::spawn(state_loop(
            Arc::clone(rx_state),
            Arc::clone(&state),
            Arc::clone(&waker),
        ));
        SocState { state, waker }
    }
}

async fn state_loop(
    rx_state: Arc<Mutex<Receiver<(u32, bool)>>>,
    state: Arc<Mutex<Option<(u32, bool)>>>,
    waker: Arc<Mutex<Option<Waker>>>,
) {
    loop {
        let res = rx_state.lock().unwrap().try_recv();
        if let Ok(message) = res {
            *state.lock().unwrap() = Some(message);
            if let Some(wake) = waker.lock().unwrap().take() {
                wake.wake();
            }
        } else {
            sleep(Duration::from_millis(500)).await;
        }
    }
}

impl Stream for SocState {
    type Item = Message;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let opt: Option<(u32, bool)> = *self.state.lock().unwrap();
        match opt {
            None => {
                self.waker.lock().unwrap().replace(cx.waker().clone());
                Poll::Pending
            }
            Some(state) => {
                *self.state.lock().unwrap() = None;
                Poll::Ready(Some(Message::ChangeState(state)))
            }
        }
    }
}

impl Recipe for SocState {
    type Output = Message;

    fn hash(&self, hasher: &mut OtherHasher) {
        if let Ok(d) = self.state.lock() {
            if let Some(state) = *d {
                state.hash(hasher);
            }
        }
    }

    fn stream(self: Box<Self>, _: EventStream) -> BoxStream<Message> {
        Box::new(self).boxed()
    }
}

async fn connect() -> SmartProtocolResult<Client> {
    let addr =
        fs::read_to_string("settings/addr").unwrap_or_else(|_| String::from("127.0.0.1:8080"));
    Client::connect(addr, "room1", "socket1").await
}

#[derive(Debug)]
pub struct Client {
    tx_com: Arc<Sender<ComSocket>>,
    rx_state: Arc<Mutex<Receiver<(u32, bool)>>>,
}

enum ComSocket {
    On,
    Off,
}

impl Client {
    pub async fn connect<Addrs: ToSocketAddrs>(
        addrs: Addrs,
        room: impl ToString,
        device: impl ToString,
    ) -> SmartProtocolResult<Self> {
        let (tx_com, rx_com) = channel::<ComSocket>();
        let (tx_state, rx_state) = channel::<(u32, bool)>();
        let rx_com = Arc::new(Mutex::new(rx_com));
        let rx_state = Arc::new(Mutex::new(rx_state));
        let connect = SmartConnection::new(TcpStream::connect(addrs).await?).await?;

        task::spawn(sender(
            connect,
            room.to_string(),
            device.to_string(),
            rx_com.clone(),
            Arc::new(tx_state),
        ));

        Ok(Client {
            tx_com: Arc::new(tx_com),
            rx_state,
        })
    }
}

async fn sender(
    mut connect: SmartConnection<TcpStream>,
    room: String,
    device: String,
    rx_com: Arc<Mutex<Receiver<ComSocket>>>,
    tx_state: Arc<Sender<(u32, bool)>>,
) {
    let power_request = Arc::new(AtomicBool::new(true));
    let toggle = AtomicBool::new(true);
    task::spawn(timer(Arc::clone(&power_request)));
    loop {
        let res = rx_com.lock().unwrap().try_recv();
        if let Ok(message) = res {
            toggle.store(
                match message {
                    ComSocket::On => true,
                    ComSocket::Off => false,
                },
                Relaxed,
            );
            send_command(&mut connect, &room, &device, message).await;
            power_request.store(true, Relaxed)
        } else {
            sleep(Duration::from_millis(50)).await;
        }
        if power_request.swap(false, Relaxed) {
            let power = get_power(&mut connect, &room, &device).await;
            tx_state.send((power, toggle.load(Relaxed))).unwrap();
        }
    }
}

async fn timer(time: Arc<AtomicBool>) {
    loop {
        sleep(Duration::from_millis(3000)).await;
        time.store(true, Relaxed);
    }
}

async fn send_command(
    connect: &mut SmartConnection<TcpStream>,
    room: &String,
    device: &String,
    command: ComSocket,
) {
    let command = match command {
        ComSocket::On => "on",
        ComSocket::Off => "off",
    };
    let command = format!("{} {} {}", room, device, command);
    if let Ok(answer) = connect.handle(command).await {
        println!("{answer}");
    }
}

async fn get_power(
    connect: &mut SmartConnection<TcpStream>,
    room: &String,
    device: &String,
) -> u32 {
    let command = format!("{} {} power", room, device);
    let idx = command.len() + 13;
    if let Ok(answer) = connect.handle(command).await {
        let answer = answer.split_at(idx).1;
        return answer.parse().unwrap_or(0);
    }
    0
}
