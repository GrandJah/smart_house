Описание/Пошаговая инструкция выполнения домашнего задания:
- [x] Разделить логически целостные элементы библиотеки ""умный дом"" на отдельные файлы.
- [x] Покрыть тестами требования к библиотеке.
- [x] Создать example использования библиотеки.


- [x] Дом имеет название и содержит несколько помещений.
- [x] Помещение имеет уникальное название и содержит названия нескольких устройств.
- [x] Устройство имеет уникальное в рамках помещения имя.


- [x] Библиотека предоставляет структуру дома в комнатах которого расположены устройства.


- [x] Библиотека позволяет запросить список помещений в доме.
- [x] Библиотека позволяет получать список устройств в помещении.
- [x] Библиотека имеет функцию, возвращающую текстовый отчёт о состоянии дома:
  - [x] функция принимает в качестве аргумента обобщённый тип, позволяющий получить текстовую информацию
о состоянии устройства, для включения в отчёт. 
      - [x] информация должна предоставляться для каждого устройства на основе данных о положении устройства в доме: имени комнаты и имени устройства.
  - [x] Если устройство не найдено в источнике информации, то вместо текста о состоянии вернуть сообщение об ошибке.

###### Обработка ошибок в прототипе "умного дома"

- [x] Функции библиотеки ""умный дом"" должны возвращать Option в случае, если есть вероятность не получить результат.
- [x] Функции библиотеки ""умный дом"" должны возвращать Result в случае, отсутствие результата может быть вызвано различными
причинами.

###### Динамический "Умный дом"

- [x] Библиотека должна предоставлять динамическую структуру дома в комнатах которого расположены устройства.
- [x] Библиотека позволяет запросить список помещений в доме, а также добавлять и удалять помещения.
- [x] Библиотека позволяет получать список устройств в помещении, а также добавлять и удалять устройства.

###### TCP-клиент умной розетки, позволяющий:

- [x] включать и выключать розетку,
- [x] запрашивать информацию о текущем состоянии и потребляемой мощности розетки.
- [x] Также, для проверки нового функционала, реализовать приложение, имитирующее работу умной розетки, управляемой по TCP.

###### UDP-клиент умного термометра, позволяющий:

- [x] термометр, периодически обновляющий данные о температуре.
- [x] получать/передавать информацию о температуре, получая их с заданного сокета по UDP.
- [x] Также, для проверки нового функционала, реализовать приложение, имитирующее работу термометра, отправляющего показания
на заданный UDP сокет.

###### Асинхронное сетевое взаимодействие
- [x] Модули для сетевого взаимодействия с умной розеткой и термометром должны предоставлять асинхронный интерфейс.
- [x] Системные потоки не должны создаваться вручную.

###### Крейт thiserror
- [x] Заменить ручную реализацию обработки ошибок в библиотеке "умный дом" на использование крейта thiserror.

###### Веб-сервер "умного дома".

Описание/Пошаговая инструкция выполнения домашнего задания:

- [x] Реализовать с использованием веб-фреймворка HTTP сервер, реализующий функционал ""Умного дома"":
- - [x] Позволяет запросить список помещений в доме, а также добавлять и удалять помещения.
- - [x] Позволяет получать список устройств в помещении, а также добавлять и удалять устройства.
- - [x] Библиотека имеет функцию, возвращающую текстовый отчёт о состоянии дома.

- [x] Написать клиент с запросами к HTTP API умного дома.
- [x] Написать example общения с умным домом через HTTP клиент.

###### Графический интерфейс "Умной розетки" Приложение "Умная розетка по TCP" с GUI.

Описание/Пошаговая инструкция выполнения домашнего задания:

- [x] Написать GUI приложение для управления "Умной розеткой" по TCP. Оно должно позволять:
- - [x] Включать/отключать розетку.
- - [x] Отображать текущее состояние розетки и потребляемую мощность.
