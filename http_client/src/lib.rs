pub use api::Api;

pub mod api;
mod fetch;

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

