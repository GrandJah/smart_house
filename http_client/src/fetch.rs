use http_body_util::BodyExt;
use hyper::body::Body;
use hyper::http::uri::{Authority, PathAndQuery, Scheme};
use hyper::{body::Buf, Method, Request, Uri};
use serde::de::StdError;
use tokio::net::TcpStream;

use crate::Result;

pub struct Client {
    addr: String,
    authority: Authority,
    scheme: Scheme,
}

impl Client {
    pub async fn new(uri: Uri) -> Result<Client> {
        let host = uri.host().expect("uri has no host");
        let port = uri.port_u16().unwrap_or(80);
        let addr = format!("{}:{}", host, port);

        let scheme = uri.scheme().unwrap().clone();
        let authority = uri.authority().unwrap().clone();

        Ok(Client {
            addr,
            scheme,
            authority,
        })
    }

    pub async fn fetch_json<T, B>(&self, url: &'static str, method: Method, body: B) -> Result<T>
    where
        T: for<'de> serde::de::Deserialize<'de>,
        B: Body + 'static + Send,
        B::Data: Send,
        B::Error: Into<Box<dyn StdError + Send + Sync>>,
    {
        let stream = TcpStream::connect(self.addr.clone()).await?;
        let io = hyper_util::rt::TokioIo::new(stream);
        let (mut sender, conn) = hyper::client::conn::http1::handshake(io).await?;
        tokio::task::spawn(async move {
            if let Err(err) = conn.await {
                println!("Connection failed: {:?}", err);
            }
        });

        let uri = Uri::builder()
            .scheme(self.scheme.clone())
            .authority(self.authority.clone())
            .path_and_query(PathAndQuery::from_static(url))
            .build()?;

        let req = Request::builder()
            .method(method)
            .uri(uri)
            .header(hyper::header::HOST, self.authority.clone().as_str())
            .body(body)?;
        let resp = sender.send_request(req).await?;
        let body = resp.collect().await?.aggregate();
        Ok(serde_json::from_reader(body.reader())?)
    }
}
