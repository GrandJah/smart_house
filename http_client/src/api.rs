use bytes::Bytes;
use http_body_util::Empty;
use hyper::Method;

use http_api::ReportRs;

use crate::fetch::Client;
use crate::Result;

pub struct Api {
    client: Client,
}

impl Api {
    pub async fn new(url: &str) -> Self {
        Api {
            client: Client::new(url.parse().unwrap()).await.unwrap(),
        }
    }

    pub async fn get_report(&self) -> Result<ReportRs> {
        self.client
            .fetch_json("/", Method::GET, Empty::<Bytes>::new())
            .await
    }
}
