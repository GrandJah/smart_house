use http_client::{Api, Result};

#[tokio::main]
async fn main() -> Result<()> {
    let api = Api::new("http://127.0.0.1:8080/").await;
    let resp = api.get_report().await?;
    println!("{}", resp.report);
    Ok(())
}
